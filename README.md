# たけびし「Device Gateway」をMicroShiftで実行する


## 環境
OS: RHEL 8.7 (RHEL for Edge)

HW: Intel NUC NUC12WSHi7

MicroShiftは、Red Hat Device Edge 4.12を使用しています。

## Getting Started
### MicroShiftのインストール
レッドハットの[公式ドキュメント](https://access.redhat.com/documentation/en-us/red_hat_build_of_microshift/4.12/html/installing/microshift-install-rpm)を参照し、Red Hat Device Edge(RHDE)をインストールします。また、本リポジトリにある[この手順](./microshift/microshift-install.md)も参考にしてください。

### Device Gatewayのコンテナイメージの取得
たけびしの[公式サイト](https://www.faweb.net/download/download/#devicegateway)でDevice Gatewayのコンテナイメージとドキュメントをダウンロードしてください。

Note. Device Gatewayを使用するにはライセンスが必要です

### Device GatewayのコンテナイメージをIntel NUC上でロード

```
## rootユーザでロードします。
sudo podman load < tkbs-dgwd20-x86_64-v310.tar.gz
sudo crictl images | grep localhost
localhost/tkbs-dgwd20-x86_64 ...
```

### Device GatewayをMicroShift上へデプロイ

```bash
oc create ns dgw
oc label --overwrite ns dgw pod-security.kubernetes.io/enforce=privileged
oc config set-context $(kubectl config current-context) --namespace=dgw
oc apply -f dgw
```

```bash
oc get po
```

```
NAME                        READY   STATUS    RESTARTS
tkbs-dgw-864f4cd7d5-rnjns   1/1     Running   1       
```

### AMQ BrokerをMicroShift上へデプロイ
```bash
oc create amq-broker
oc config set-context $(kubectl config current-context) --namespace=amq-broker
oc apply -f amq-broker
```

```bash
oc get po
```

```
NAME                          READY   STATUS    RESTARTS
amq-broker-5578b4f8df-5bzp9   1/1     Running   1       
```

```bash
oc get svc
```

```
NAME        TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)       
amq-web     NodePort   10.43.198.125   <none>        8161:30221/TCP
amqp        NodePort   10.43.83.163    <none>        5672:31218/TCP
amqp-mqtt   NodePort   10.43.6.69      <none>        1883:32309/TCP
```

### Device Gatewayの設定
#### データソースの通信設定
![データソースの通信設定](./images/datasource-comm.png) 
#### データソースのタグ設定
![データソースの通信設定](./images/datasource-comm.png)
#### イベントのアクション設定
![データソースの通信設定](./images/event-action.png) 


```JSON
{
   "time":  <%=@nowUtc("yyyyMMdd HH:mm:ss.ms")%>,
   "status": <%=@p("Managemnt.Information.Summary.ledStatus")%>,
   "send": <%=@p("Managemnt.Lan.Summary.sendBytes0")%>,
   "recieve": <%=@p("Managemnt.Lan.Summary.receiveBytes0")%>
}
```

#### 上位連携設定(MQTT)
![データソースの通信設定](./images/mqtt.png)
### MQTTの送信状況の確認

```bash
$ mosquitto_sub -h <NodeIP> -p <NodePort> -t DEVGW
```

```JSON
"Status": true
"Time": 20230208 09:05:55.745
"LED": 緑点灯（エラー無し）
"Error": 0
"Count":0
}
{

"Status": true
"Time": 20230208 09:05:56.745
"LED": 緑点灯（エラー無し）
"Error": 0
"Count":0
}
```


```JSON
{
	"Equipment_State": <%=@tq("melsec.データレジスタ")%>,
        "D0": <%=@tv("melsec.データレジスタ")%>,
        "Good_count": <%=@p("DataSource.melsec.Summary.sendCount")%>,
        "Error_count": <%=@p("DataSource.melsec.Summary.errorCount")%>,
}
```