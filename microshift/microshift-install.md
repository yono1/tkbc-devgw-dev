## RHEL for Edgeのインストール
- Red Hat Hybrid Cloud ConsoleでRHEL for EdgeのOSイメージをビルド([こちら](https://access.redhat.com/documentation/ja-jp/red_hat_enterprise_linux/8/html/composing_installing_and_managing_rhel_for_edge_images/introducing-rhel-for-edge-images_composing-installing-managing-rhel-for-edge-images)を参照)
- 作成したOSイメージをUSBに焼く(ラズパイビルダーを使用)
- 作成したOSイメージを使用してIntel NUCへOSインストール([F10]を押してブートメニューでRHELインストールを選択)

## OSのセットアップ
### SSHログイン
RHEL for Edgeイメージを作成する際に指定した公開鍵に対応する秘密鍵を使用してsshします。

```
$ ssh -i ~/.ssh/id_rsa microshift@<IPアドレス>
[microshift@localhost ~]$
```

### ホスト名とパスワードを変更
ホスト名は後述のmDNSに対応できるように、`<任意の名前>.local`の形式としておきます。

```
[microshift@rhde ~]$ sudo hostnamectl set-hostname rhde.local
[microshift@rhde ~]$ sudo passwd microshift
```

### Persistent Volumeで使用するVolume Groupの作成
```
[microshift@rhde ~]$ sudo su
bash-4.4# fdisk /dev/nvme0n1

Command (m for help): p

Device            Start       End   Sectors  Size Type
/dev/nvme0n1p1     2048   1230847   1228800  600M EFI System
/dev/nvme0n1p2  1230848   3327999   2097152    1G Linux filesystem
/dev/nvme0n1p3  3328000  68974591  65646592 31.3G Linux swap
/dev/nvme0n1p4 68974592 215775231 146800640   70G Linux filesystem

Command (m for help): n 
Partition number (5-128, default 5): [Enter]
First sector (215775232-1953525134, default 215775232): [Enter]
Last sector, +sectors or +size{K,M,G,T,P} (215775232-1953525134, default 1953525134): [Enter]

Created a new partition 5 of type 'Linux filesystem' and of size 828.6 GiB.
Partition #5 contains a xfs signature.

Do you want to remove the signature? [Y]es/[N]o: Yes

Command (m for help): p

Device             Start        End    Sectors   Size Type
/dev/nvme0n1p1      2048    1230847    1228800   600M EFI System
/dev/nvme0n1p2   1230848    3327999    2097152     1G Linux filesystem
/dev/nvme0n1p3   3328000   68974591   65646592  31.3G Linux swap
/dev/nvme0n1p4  68974592  215775231  146800640    70G Linux filesystem
/dev/nvme0n1p5 215775232 1953525134 1737749903 828.6G Linux filesystem

Filesystem/RAID signature on partition 5 will be wiped.

Command (m for help): w

[microshift@rhde ~]$ sudo pvcreate /dev/nvme0n1p5 
  Physical volume "/dev/nvme0n1p5" successfully created.

## MicroShiftではStorage ClassでTopolvmを使用できます。デフォルトで「rhel」という名前のVolume Groupを使用します。
[microshift@rhde ~]$ sudo vgcreate rhel /dev/nvme0n1p5 
  Volume group "microshift_vg" successfully created

[microshift@rhde ~]$ sudo vgs
  VG            #PV #LV #SN Attr   VSize   VFree  
  microshift_vg   1   0   0 wz--n- 828.62g 828.62g
```


### Subscription Managerへ登録
```
[microshift@rhde ~]$ sudo subscription-manager register
You are attempting to use a locale: "ja_JP.UTF-8" that is not fully supported by this system.
登録中: subscription.rhsm.redhat.com:443/subscription
ユーザー名: 
パスワード: 
```

### MicroShiftのインストール

rpm-ostreeでインストールします。

```
[microshift@rhde ~]$ sudo rpm-ostree install microshift
```

### mDNSで必要なパッケージをインストール
mDNSは、同一LAN内の`<ホスト名>.local`のドメインを名前解決する機能です。MicroShiftはmDNSがビルトインされており、Routeを`<ホスト名>.local`のホスト名で作成するとmDNSの対象とできます。
mDNSの使用には、ホスト側でavahiとnss-mdnsをインストールします。なお、nss-mdnsのインストールにはepelリポジトリが必要です。

```
[microshift@rhde ~]$ sudo rpm-ostree install avahi
[microshift@rhde ~]$ sudo rpm-ostree install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm

## リブートしてインストールパッケージを有効化します。
[microshift@rhde ~]$ sudo systemctl reboot
```

```
### OS起動後にnss-mdnsをインストール
[microshift@rhde ~]$ sudo rpm-ostree install nss-mdns
[microshift@rhde ~]$ sudo systemctl reboot
```

### レッドハットのコンテナイメージのPull Secretを配置

```
$ scp pull-secret.txt microshift@<IPアドレス>:~/
```

```
[microshift@rhde ~]$ sudo cp pull-secret.txt /etc/crio/openshift-pull-secret
[microshift@rhde ~]$ sudo chown root:root /etc/crio/openshift-pull-secret
[microshift@rhde ~]$ sudo chmod 600 /etc/crio/openshift-pull-secret
```

### Firewallの設定
```
[microshift@rhde ~]$ sudo firewall-cmd --permanent --zone=trusted --add-source=10.42.0.0/16
[microshift@rhde ~]$ sudo firewall-cmd --permanent --zone=trusted --add-source=169.254.169.1
[microshift@rhde ~]$ sudo firewall-cmd --permanent --add-port=30000-32767/tcp
[microshift@rhde ~]$ sudo firewall-cmd --zone=public --add-port=5353/udp --permanent
[microshift@rhde ~]$ sudo firewall-cmd --reload
```

### MicroShiftの起動

```
[microshift@rhde ~]$ sudo systemctl start microshift
[microshift@rhde ~]$ sudo systemctl enable microshift
```

Note /usr配下がroでマウントされている場合、MicroShiftのコアコンポーネントの起動に失敗します。WAとして事前にrwでリマウントしてください。

```
[microshift@rhde ~]$ sudo mount | grep usr
/dev/nvme0n1p4 on /usr type xfs (ro,relatime,seclabel,attr2,inode64,logbufs=8,logbsize=32k,noquota)
[microshift@rhde ~]$ sudo mount -o remount,rw /usr
[microshift@rhde ~]$ sudo mount | grep usr
/dev/nvme0n1p4 on /usr type xfs (rw,relatime,seclabel,attr2,inode64,logbufs=8,logbsize=32k,noquota)
```

### OpenShiftのCLIツールをダウンロード

```
[microshift@rhde ~]$ curl -O https://mirror.openshift.com/pub/openshift-v4/$(uname -m)/clients/ocp/stable/openshift-client-linux.tar.gz
[microshift@rhde ~]$ sudo tar -xf openshift-client-linux.tar.gz -C /usr/local/bin oc kubectl
[microshift@rhde ~]$ mkdir ~/.kube
[microshift@rhde ~]$ sudo cat /var/lib/microshift/resources/kubeadmin/kubeconfig > ~/.kube/config
```


### MicroShiftの起動を確認

```
## oc get po -A
NAMESPACE                  NAME                                  READY   STATUS    RESTARTS   AGE
openshift-dns              dns-default-6jqzt                     2/2     Running   0          3m48s
openshift-dns              node-resolver-9rmsq                   1/1     Running   0          24m
openshift-ingress          router-default-d6cc9845f-tzrkl        1/1     Running   0          24m
openshift-ovn-kubernetes   ovnkube-master-tt9ls                  4/4     Running   0          4m1s
openshift-ovn-kubernetes   ovnkube-node-9k89m                    1/1     Running   0          24m
openshift-service-ca       service-ca-5d96446959-x6w4w           1/1     Running   0          24m
openshift-storage          topolvm-controller-677cd8d9df-s9d8p   4/4     Running   0          24m
openshift-storage          topolvm-node-66vc8                    4/4     Running   0          3m48s
```

### mDNSの設定
```
[microshift@rhde ~]$ sudo systemctl start avahi-daemon
[microshift@rhde ~]$ sudo systemctl enable avahi-daemon

[microshift@rhde ~]$ sudo echo .local > /etc/mdns.allow
[microshift@rhde ~]$ sudo echo .local. >> /etc/mdns.allow
[microshift@rhde ~]$ sudosed -i 's/mdns4_minimal/mdns/g' /etc/nsswitch.conf
```